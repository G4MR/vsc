package vsc

import "strings"

type Api struct {
	Id     string
	Secret string
}

func (a Api) GetCreds() string {
	if len(a.Id) > 0 && len(a.Secret) > 0 {
		builder := strings.Builder{}
		builder.WriteString("?client_id=")
		builder.WriteString(a.Id)
		builder.WriteString("&client_secret=")
		builder.WriteString(a.Secret)
		return builder.String()
	}

	return ""
}
