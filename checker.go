package vsc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type RepoData struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

func VGOSupport(user, repo string, api Api) bool {
	apiPath := "https://api.github.com/repos/{REPO}/contents"
	apiPath = strings.Replace(apiPath, "{REPO}", strings.Trim(user+"/"+repo, "/"), -1)
	apiPath += api.GetCreds()

	files, err := getRepoData(apiPath)
	if err != nil {
		return false
	}

	for _, file := range files {
		if file.Type != "file" {
			continue
		}

		if file.Name == "go.mod" {
			return true
		}
	}

	return false
}

func getRepoData(repo string) ([]RepoData, error) {
	resp, err := http.Get(repo)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Problem loading page: %d, %s", resp.StatusCode, resp.Status)
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("Issue reading body %s", err.Error())
	}

	var files []RepoData
	err = json.Unmarshal(content, &files)
	if err != nil {
		return nil, fmt.Errorf("Issue unmarshalling data %s", err.Error())
	}

	return files, nil
}
